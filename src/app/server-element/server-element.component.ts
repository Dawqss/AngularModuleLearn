import {
  Component, OnInit, Input, OnChanges, SimpleChanges, DoCheck, AfterContentInit,
  AfterContentChecked, ViewChild, ElementRef, AfterViewInit, ContentChild
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit,
    OnChanges,
    DoCheck,
    AfterContentChecked,
    AfterViewInit {

  @Input('srvElement') element: {type: string, name: string, content: string};
  @Input() name: string;
  @ViewChild('heading') header: ElementRef;
  @ContentChild('contentParagraph') paragraph: ElementRef;

  constructor() {
    console.log('constructor called');
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChagnes called');
    console.log(changes);
  }

  ngOnInit() {
    console.log('NgInit called');

  }

  ngDoCheck() {
    console.log('ngDoCheckCalled!');
  }

  ngAfterContentChecked() {
    console.log('MotherFUckj init')
  }

  ngAfterViewInit() {
    console.log(this.header.nativeElement.textContent);
  }
}
